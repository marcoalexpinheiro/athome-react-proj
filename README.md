** SOME CONSIDERATIONS **
This was my first hands-on REACT project and I enjoyed it a lot;
Every configurations lives on config.js file to improove organisation and future updates (API endpoint, assets URLs, etc.)
CSS: poniting to the bundled .CSS file in production (https://www.athome.lu/portal-srp/home-1cd94d78767f0920d180.css)

** GIT REPO **
The source code is hosted on [Bitbucket](https://bitbucket.org/marcoalexpinheiro/athome-react-proj)
git clone git@bitbucket.org:marcoalexpinheiro/athome-react-proj.git

** ONLINE TEST DRIVE **
You can test in right now [here](https://mp.elsasantos.pt)

** DEPENDENCIES **
"react": "^16.5.2",
"react-dom": "^16.5.2",
"react-scripts": "2.0.5",
"react-simple-lightbox": "^1.0.26"

** EXTRAS **
- simples preloader
- made each company propriety a Component (Sqare.js)
- made a ActionBar component (ActionBar.js)
- implemented a little search box with two way databinding fields but on the end, I realised that the API doesn't take into consideration the min_price and max_price sent... :)

** yarn commands **
"yarn install" to install node dependencies
"yarn start" to run in dev mode
"yarn run build" to build for production and deploy