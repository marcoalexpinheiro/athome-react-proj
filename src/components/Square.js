import React, { Component } from "react";
import Lightbox from "react-simple-lightbox";

class Square extends Component {
    
    constructor(props) {
      super(props);
    }
    
    render() {
    
        return (
            <div key={this.props.offer.id} className="square">
            <picture>
                
                <Lightbox>
                <img
                    src={
                    this.props.pic_repo +
                    this.props.offer.picture
                    }
                    alt={this.props.offer.price + "€ " + this.props.offer.city}
                />
                </Lightbox>
                
            </picture>
            <div className="showcase-informations">
                <p className="price">
                    {this.props.offer.price > 0 ? this.props.offer.price + " €": "prix sur demande" }
                </p>
                <p className="city">{this.props.offer.city}</p>
            </div>
            </div>

        );
    }
}

export default Square;