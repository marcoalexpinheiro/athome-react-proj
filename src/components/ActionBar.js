import React, { Component } from "react";

class ActionBar extends Component {
    
    constructor(props) {
      super(props);
    }
    
    render() {
        return (
            <div className="actions-bar">
                <div className="filterItem">
                    <button className="alerte" onClick={this.props.fetchData.bind(this)}>
                    cherchez
                    </button>
                </div>
                <div className="filterItem">
                    <label className="filter-label">min prix</label> <input className="filter-input-field" name="min_price" type="number" value={this.props.params.search.min_price} onChange={ this.props.handleChangeMinPrice.bind(this) } />
                </div>
                <div className="filterItem">
                    <label className="filter-label">max prix</label> <input className="filter-input-field" name="max_price" type="number" value={this.props.params.search.max_price} onChange={ this.props.handleChangeMaxPrice.bind(this) } />
                </div>
            </div>
        );
    }
}

export default ActionBar;