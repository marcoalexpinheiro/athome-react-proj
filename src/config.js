export const CONFIG = {
  api_endpoint:
    "https://www.athome.lu/api/athome/suggested-showcase/v/2/key/368a870629c7ba6b778befd6f2da3309",
  api_headers: {
    "Content-Type": "application/x-www-form-urlencoded",
    Connection: "keep-alive"
  },

  client_logos_url: "https://i1.static.athome.eu/logoagences/",
  agence_base_url: "https://www.athome.lu/agence/",
  agence_props_dir_url: "https://www.athome.lu/srp/?pid=",
  property_repo_url: "https://i1.static.athome.eu//images/annonces2/image_/",

  api_query_obj: {
    site: "athome.lu",
    home: 1,
    transaction: "by",
    search: {
      property_group: ["flat"],
      property_type: [],
      min_price: "300000",
      max_price: "0"
    },
    related: {}
  }
};
