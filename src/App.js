import React, { Component } from "react";
import Square from "./components/Square";
import ActionBar from "./components/ActionBar";
import { CONFIG } from './config.js';

import "./App.css";

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      showcase_list: [],
      isLoading: true,
      params: CONFIG.api_query_obj
    };
  }

  handleChangeMinPrice(e) {
    let params = Object.assign({}, this.state.params);
    params.search.min_price = e.target.value;
    this.setState({params});
  }

  handleChangeMaxPrice(e) {
    let params = Object.assign({}, this.state.params);
    params.search.max_price = e.target.value;
    this.setState({params});
  }

  fetchData(){

    this.setState({ isLoading: true });

    fetch(CONFIG.api_endpoint, {
      method: "POST",
      headers: new Headers(CONFIG.api_headers),
      body: "params=" + JSON.stringify(this.state.params)
    })
      .then(response => {
        if (response.ok) {
          return response.json();
        } else {
          throw new Error("Error fetching API...");
        }
      })
      .then(showcase_list => this.setState({ showcase_list, isLoading: false }))
      .catch(error => this.setState({ error, isLoading: false }));
  }

  componentDidMount() {
    this.setState({ isLoading: true });
    this.fetchData();
  }

  render() {
    const { showcase_list, isLoading } = this.state;

    return (
      <div className="App showcase intro">
        <div id="preloader" className={isLoading ? "fadeIn" : "fadeOut"}>
          <div className="inner">
            <img alt="fetching info..." src="logo_hp_header.svg" />
            <span>fetching API ... </span>
          </div>
        </div>

        <header className="container-logo">
          <img alt="atHome" src="https://www.athome.lu/portal-srp/assets/images/lu/logo.svg" />
        </header>

        <ActionBar params={this.state.params} handleChangeMaxPrice={this.handleChangeMaxPrice.bind(this)} handleChangeMinPrice={this.handleChangeMinPrice.bind(this)} fetchData={this.fetchData.bind(this)} />
        
        {showcase_list.map(company => (
          <div key={company.id} className="picture-gallery">
            <div className="gallery">
              <div className="gallery-content">
                <ul>
                  {company.offers.map(offer => (
                    <li key={offer.id}>
                      <Square pic_repo={CONFIG.property_repo_url} offer={offer} />
                    </li>
                  ))}
                </ul>
              </div>
            </div>

            <div className="informations">
              <a href={CONFIG.agence_base_url + company.customerId}>
                <img alt="foo" src={CONFIG.client_logos_url + company.logo} />
              </a>
              <div className="adress">
                <p>{company.customer}</p>
                <p>
                  {company.street} {company.zip} {company.city}
                </p>
              </div>
              <a
                href={CONFIG.agence_props_dir_url + company.customerId}
                className="nbr-annonces"
              >
                {"Voir " + company.countOffer + " annonces"}
              </a>
            </div>
          </div>
        ))}
      </div>
    );
  }
}

export default App;
